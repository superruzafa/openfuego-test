class openfuego::mysql(
  $root_password = 'openfuego-root',
  $user = 'openfuegoa',
  $user_password = 'openfuego-admin',
  $database = 'openfuego',
)
{
  package { 'mysql-server':
    ensure => present
  }

  service { 'mysql':
    ensure => running,
    enable => true,
    subscribe => Package['mysql-server']
  }

  exec { 'root_password':
    require => Service['mysql'],
    unless => "mysqladmin -u root -p$root_password status",
    command => "mysqladmin -u root password $root_password",
  }

  exec { 'create_db':
    require => Exec['root_password'],
    command => "echo CREATE DATABASE IF NOT EXISTS $database | mysql -u root -p$root_password",
  }

  exec { 'create_user':
    require => Exec['create_db'],
    command => "echo GRANT CREATE,ALTER,DROP,INDEX,SELECT,INSERT,DELETE,UPDATE ON $database.* TO $user@localhost IDENTIFIED BY '\"'$user_password'\"' | mysql -uroot -p$root_password $database"
  }
}
