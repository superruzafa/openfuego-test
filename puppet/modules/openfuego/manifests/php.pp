class openfuego::php
{
  $php_packages = ['php5-cli', 'php5-curl', 'php5-mysql']
  package { $php_packages:
    ensure => present,
  }
}
